import React from 'React'
import { Button } from 'react-native';

const ButtonElement = props => {
  return(
    <Button
      title="Learn More"
      onPress={props.onPressLearnMore}
      accessibilityLabel="Learn more about this purple button"
    />
  );
}
export default ButtonElement;
