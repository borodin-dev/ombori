import React from 'react';
import { View, Animated, Easing, Text, Image, FlatList } from 'react-native';
import { List, ListItem } from "react-native-elements";
import PropTypes from 'prop-types';
import Pulse from './main/Pulse';


export default class App extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			circles: [],
      userData: [],
      page: 1,
      total_pages: Infinity,
		};

		this.counter = 1;
		this.setInterval = null;
		this.anim = new Animated.Value(1);
	}

	componentDidMount() {
		this.setCircleInterval();
    this.load();
	}

  load() {
    this.loadUsers();
  }

	setCircleInterval() {
		this.setInterval = setInterval(this.addCircle.bind(this), this.props.interval);
		this.addCircle();
	}

	addCircle() {
		this.setState({ circles: [...this.state.circles, this.counter] });
		this.counter++;
	}

  loadUsers(){
    let url = `https://reqres.in/api/users?page=${this.state.page}`
    fetch(url)
      .then((response)=> {

        return response.json();
       })
      .then((users)=> {
        this.setState((prevState)=>{
          return {
            userData: [...prevState.userData, ...users.data],
            page: prevState.page + 1,
            total_pages: users.total_pages
          }
        })
        // console.log('data  ', ...users.data);

      })
      // .finally(() => {
      //   if(this.state.total_pages >= this.state.page) {
      //     this.load()
      //   }
      // })
      .catch(alert);
    }

	render() {
		const { size, bgColor, interval } = this.props;

		return (
			<List >
        <FlatList
          data={this.state.userData}
          renderItem={({item}) => (
            <ListItem
              roundAvatar
              title={`${item.first_name} ${item.last_name}`}
              avatar={{ uri: item.avatar }}
              chevronColor="#f0f"
              hideChevron
            />
          )}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={() =>{this.load()}}
          onEndReachedThreshold={0.1}
        />
    </List>
		);
	}
}

App.propTypes = {
  interval: PropTypes.number,
  size: PropTypes.number,
  pulseMaxSize: PropTypes.number,
  backgroundColor: PropTypes.string,
  bgColor: PropTypes.string,
};

App.defaultProps = {
  interval: 1500,
  size: 20,
  pulseMaxSize: 140,
  backgroundColor: '#cee4a5',
  bgColor: '#79b72c',
};
